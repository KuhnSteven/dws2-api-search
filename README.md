# README #
Project Goals:
    HTML/SCSS
Variables for font family, font size, and an action color.
One mixin for a useful, reusable segment of styles.
Two uses of nesting, but never more than three levels deep.
One placeholder (%) and at least three uses of extend.
A folder structure consisting of at least 3 folders, 5 partial files, imported into a single main scss file.
    Gulp Automation
Demonstrate a knowledge of creating a task and using the watch functionality with gulp.
Feel free to use any other gulp tasks or plugins you find useful.
    API Results
Capture the search query from the search box and build the request url.
Make an AJAX request to the API endpoint you’ve chosen.
Generate the results to match useful data from the API, conforming to the general layout provided.
Think about how the page might look before a search has been performed, or if there are no results, and design accordingly.

### What is this repository for? ###

* Quick summary:
This repo's code is intended to search through a linked database via API, and return results to the user on the webpage/html
* Version

### Who do I talk to? ###

* Repo owner or admin (Steven Kuhn)